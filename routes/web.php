<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/videos', function () {
    return view('videos');
});

Route::get('/temoignages', function () {
    return view('temoignages');
});

Route::get('/vitabu','VitabuController@index');

Route::get('/articles', 'ArticlesController@index');

Route::get('/article/{slug}',[
    'as'=>'article.single',
    'uses'=>'ArticlesController@single'
]);

Route::get('/formations', function () {
    return view('formations');
});
Route::get('/forum', function () {
    return view('forum');
});
