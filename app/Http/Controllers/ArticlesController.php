<?php

namespace App\Http\Controllers;

use App\Articles;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index() {
        $articles = Articles::all();
        return View('articles',compact('articles'));
    }

    public function single($slug) {
        $article = Articles::where('slug',$slug)->first();
        return View('singleArticle',compact('article'));
    }
}
