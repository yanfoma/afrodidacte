<?php

namespace App\Http\Controllers;

use App\Vitabu;
use Illuminate\Http\Request;

class VitabuController extends Controller
{
    public function index() {
        $vitabus = Vitabu::all();
        return View('vitabu',compact('vitabus'));
    }
}
