<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    //
    protected $attributes = array(
	   'is_active' => true,
	);
}
