<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_admin');// to define if the use has the right to acces admin pannel
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('formations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formation_logo');
            $table->string('name');
            $table->integer('duration');
            $table->longText('description');
            $table->timestamps();
        });

        Schema::create('testimonies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('person_img');
            $table->string('description');
            $table->text('content');
            $table->boolean('is_active');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('formations');
        Schema::dropIfExists('testimonies');
        
    }
}
