<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVitabuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( !Schema::hasTable('vitabu') ) {
            Schema::create('vitabu', function (Blueprint $table) {
                $table->increments('id');
                $table->string('titre');
                $table->string('auteur');
                $table->text('lien');
                $table->text('image');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('vitabu') ) {
            Schema::dropIfExists('vitabu');
        }
    }
}
