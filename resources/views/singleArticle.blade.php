@extends('layout')

@section('article_meta')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"              content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="{{$article->title}}" />
    <meta property="og:site_name" 	   content="Afrodidacte">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="{{$article->title}}" />
    <meta property="og:description"    content="{{$article->title}}" />
    <meta property="og:image"          content="{{asset($article->image)}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="">
    <meta name="twitter:card"          content="summary">
    <meta name="twitter:site"          content="@afrodidacte">
    <meta name="twitter:title"         content="{{$article->title}}">
    <meta name="twitter:description"   content="{{$article->title}}">
    <meta name="twitter:image"         content="{{asset($article->image)}}">
@endsection

@section('content')

    <style type="text/css">

        #share-buttons img {
            width: 35px;
            padding: 5px;
            border: 0;
            box-shadow: 0;
            display: inline;
        }

    </style>


<div id="content">

    <div class="container" style="height:400px">
        <div class="col-sm-12" id="blog-post">
            <div class="box">

                <h1>{{$article->title}}</h1>
                <p class="author-date">{{$article->created_at}}</p>
                <div id="post-content">
                    {!! $article->content !!}
                </div>
                <div id="share-buttons">
                    <h2>Partager</h2>
                    <!-- Facebook -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=&t="                           title="Afrodidacte" 	onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;">
                        <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                    </a>
                    <!-- Twitter -->
                    <a href="https://twitter.com/intent/tweet?"                                          title="Afrodidacte" 	onclick="window.open('https://twitter.com/intent/tweet?text=' + ':%20 ' + encodeURIComponent(document.URL)); return false;" data-hashtags="Afrodidacte">
                        <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                    </a>

                    <!-- LinkedIn -->
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=" title="Afrodidacte"    onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' + encodeURIComponent(document.title)); return false;">
                        <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
                    </a>

                    <!-- Google+ -->
                    <a href="https://plus.google.com/share?url="                                           title="Afrodidacte" 	 onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;">
                        <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
                    </a>

                    <!-- Email -->
                    <a href="mailto:?subject=&body=:%20"                                                    title="Afrodidacte"   onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' + encodeURIComponent(document.URL)); return false;">
                        <img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email" />
                    </a>
                </div>
                <!-- /#post-content -->
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     var disqus_config = function () {
                     this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                     };
                     */
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://afrodidacte.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>

@stop
