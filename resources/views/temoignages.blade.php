@extends('layout')

@section('content')

<div id="content">

    <div class="container">
    <h2 style="color: #751008" >Ils témoignent ...</h2>

    <hr style="height:0.5px; background-color: #751008;"/>
        <div class="row temoignages_block ">

                <div class="col-md-9 col-lg-9">
                    <div class='row'>
                        <p class='temoignage_user_name'>
                            Alexa Drent, Yaounde Cameroun
                        </p>
                    </div>
                    <div class='row'>
                        <p class='temoignages_text'>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a velit ante.
                            Aliquam dolor velit, gravida quis porttitor ac, aliquet ut leo.
                            Mauris porta nunc felis. Cras et urna quis orci rutrum bibendum in at sem.
                            Aenean at tincidunt elit. In est nisl, faucibus a suscipit sit amet, placerat eu nibh.
                            Sed a felis nec enim fermentum venenatis.
                        </p>
                    </div>

                </div>
                <div class="col-md-3 col-lg-3">
                    <img class='temoignages_user_img' src="http://media.nj.com/ledgerupdates_impact/photo/2017/03/31/Wigenton.JPG" alt="user_profile_photo">
                </div>
        </div>
        <div class="row temoignages_block ">
                <div class="col-md-3 col-lg-3">
                    <img class='temoignages_user_img' src="https://www.engineering.cornell.edu/engineering/customcf/iws_ai_faculty_display/ai_images/caa238-profile.jpg" alt="user_profile_photo">
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class='row'>
                        <p class='temoignage_user_name'>
                            Adonis Morin, Yaounde Cameroun
                        </p>
                    </div>
                    <div class='row'>
                        <p class='temoignages_text'>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a velit ante.
                            Aliquam dolor velit, gravida quis porttitor ac, aliquet ut leo.
                            Mauris porta nunc felis. Cras et urna quis orci rutrum bibendum in at sem.
                            Aenean at tincidunt elit. In est nisl, faucibus a suscipit sit amet, placerat eu nibh.
                            Sed a felis nec enim fermentum venenatis.
                        </p>
                    </div>

                </div>

        </div>
    </div>

</div>

@stop
