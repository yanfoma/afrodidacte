@extends('layout')

@section('content')

<div id="content">

    <div class="container">

      <div class="col-md-12">

        @if($articles->count())
            <h2 style="color: #751008" >Mes Articles</h2>
                <hr style="height:0.5px; background-color: #751008;"/>
                <div class="col-sm-12" id="blog-listing">
                    @foreach($articles as $article)
                    <div class="post">
                        <h2 >
                            <a href="{{ route('article.single',$slug=$article->slug) }}">{{str_limit($article->title, $limiit=50,$end='...')}}</a></h2>
                        <hr>
                        <p class="date-comments">
                            <a href="#">
                                <i class="fa fa-calendar-o"></i> {{$article->created_at}} {{$article->image}}
                            </a>
                        </p>
                        @if($article->image)
                        <div class="image">
                            <a href="{{ route('article.single',$slug=$article->slug) }}">
                                <img src="{{$article->image}}" class="img-responsive" alt="{{$article->title}}">
                            </a>
                        </div>
                        @endif
                        <p class="intro">
                            {!! str_limit($article->content, $limiit=550,$end='...') !!}
                        </p>
                        <p class="read-more">
                            <a href="{{ route('article.single',$slug=$article->slug) }}" class="btn btn-primary">Lire Plus</a>
                        </p>
                    </div>
                    @endforeach
                </div>
        @else
            <h2 style="color: #751008" >Aucun Article</h2>

            <hr style="height:0.5px; background-color: #751008;"/>
        @endif
      </div>
    </div>


</div>

@stop
