
    <!-- *** NAVBAR ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="/" data-animate-hover="bounce">
                    <!-- <img src="img/rsz_1rsz_logo.png" alt="Afrodidacte logo" class="hidden-xs">
                    <img src="img/rsz_1rsz_logo.png" alt="Afrodidacte logo" class="visible-xs"><span class="sr-only">Afrodidacte - go to homepage</span> -->
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">2 items in cart</span>
                    </a> -->
                </div>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-left">
                    <li class="{{ Request::is('/') ? "active" : "" }}">
                      <a href="/">Accueil</a>
                    </li>
                    <!-- class="dropdown yamm-fw" -->
                    <li class="{{ Request::is('videos') ? "active" : "" }}">
                      <!-- class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200" -->
                        <a href="/videos">Vidéos
                          <!-- <b class="caret"></b> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li> -->
                                <!-- <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3"> -->
                                            <!-- <h5><a style="text-decoration:none" href="#">Histoire de Kama</a></h5> -->
                                            <!-- <ul>
                                                <li><a href="https://www.youtube.com/watch?v=Iodtnt2H3h0&t=19s">Une Mensonge depuis de 500 ans</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=hk1Y1ZpBRCE&t=14s">5 Faits inconnus sur l'Afrique</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=C0GTdnqQ8dE&t=13s">4 Faits inconnus sur la communaute noire</a>
                                                </li>
                                                <li><a href="#">Tout sur Kama</a>
                                                </li>
                                            </ul> -->
                                        <!-- </div>
                                        <div class="col-sm-3">
                                            <h5><a href="#" style="text-decoration:none">Entreprenariat/Business</a></h5>
                                            <ul>
                                                <li><a href="https://www.youtube.com/watch?v=aWFJInrGQLQ">Ceux qui vont developper l'Afrique</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=61q73sSmvzQ">L'investissement le plus rentable</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=kyEwUsuaR7o">Comment gagner sa liberté financière</a>
                                                </li> -->
                                                <!-- <li><a href="#">Tout sur l'entreprenariat</a>
                                                </li> -->
                                            <!-- </ul>
                                        </div> -->
                                        <!-- <div class="col-sm-3">
                                            <h5>Accessories</h5>
                                            <ul>
                                                <li><a href="category.html">Trainers</a>
                                                </li>
                                                <li><a href="category.html">Sandals</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                                <li><a href="category.html">Casual</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                                <li><a href="category.html">Casual</a>
                                                </li>
                                            </ul>
                                        </div> -->
                                        <!-- <div class="col-sm-3">
                                            <h5><a href="#" style="text-decoration:none">Developpement personnel</a></h5>
                                            <ul>
                                                <li><a href="https://www.youtube.com/watch?v=I3nPezHwNQ8">Comment apprehender le racisme</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=M54F2296nQA">Accomplissez votre mission</a>
                                                </li>
                                                <li><a href="https://www.youtube.com/watch?v=y5zroq1LrhU">L'eveil de l'Afrique</a>
                                                </li>
                                            </ul> -->
                                             <!-- <h5>Looks and trends</h5>
                                            <ul>
                                                <li><a href="category.html">Trainers</a>
                                                </li>
                                                <li><a href="category.html">Sandals</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                            </ul> -->
                                        <!-- </div>
                                    </div>
                                </div> -->
                                 <!-- /.yamm-content -->
                            <!-- </li>
                        </ul>
                    </li> -->
                    <li class="{{ Request::is('vitabu') ? "active" : "" }}">
                        <a href="/vitabu">Vitabu</a>
                    </li>
                    <li class="{{ Request::is('articles') ? "active" : "" }}">
                        <a href="/articles">Articles
                        </a>
                    </li>
                    <li class="{{ Request::is('temoignages') ? "active" : "" }}">
                        <a href="/temoignages">Témoignages
                        </a>
                    </li>
                    <li class="{{ Request::is('formations') ? "active" : "" }}">
                        <a href="/formations">Formations
                        </a>
                    </li>
                    <li class="{{ Request::is('forum') ? "active" : "" }}">
                        <a href="/forum">Forum
                        </a>
                    </li>
                    <!-- <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Vitabu <b class="caret"></b></a> -->
                        <!-- <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5>Clothing</h5>
                                            <ul>
                                                <li><a href="category.html">T-shirts</a>
                                                </li>
                                                <li><a href="category.html">Shirts</a>
                                                </li>
                                                <li><a href="category.html">Pants</a>
                                                </li>
                                                <li><a href="category.html">Accessories</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Shoes</h5>
                                            <ul>
                                                <li><a href="category.html">Trainers</a>
                                                </li>
                                                <li><a href="category.html">Sandals</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                                <li><a href="category.html">Casual</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Accessories</h5>
                                            <ul>
                                                <li><a href="category.html">Trainers</a>
                                                </li>
                                                <li><a href="category.html">Sandals</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                                <li><a href="category.html">Casual</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                                <li><a href="category.html">Casual</a>
                                                </li>
                                            </ul>
                                            <h5>Looks and trends</h5>
                                            <ul>
                                                <li><a href="category.html">Trainers</a>
                                                </li>
                                                <li><a href="category.html">Sandals</a>
                                                </li>
                                                <li><a href="category.html">Hiking shoes</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="banner">
                                                <a href="#">
                                                    <img src="img/banner.jpg" class="img img-responsive" alt="">
                                                </a>
                                            </div>
                                            <div class="banner">
                                                <a href="#">
                                                    <img src="img/banner2.jpg" class="img img-responsive" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 /.yamm-content
                            </li>
                        </ul> -->
                    <!-- </li> -->

                    <!-- <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Articles <b class="caret"></b></a> -->
                        <!-- <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5>Shop</h5>
                                            <ul>
                                                <li><a href="index.html">Homepage</a>
                                                </li>
                                                <li><a href="category.html">Category - sidebar left</a>
                                                </li>
                                                <li><a href="category-right.html">Category - sidebar right</a>
                                                </li>
                                                <li><a href="category-full.html">Category - full width</a>
                                                </li>
                                                <li><a href="detail.html">Product detail</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>User</h5>
                                            <ul>
                                                <li><a href="register.html">Register / login</a>
                                                </li>
                                                <li><a href="customer-orders.html">Orders history</a>
                                                </li>
                                                <li><a href="customer-order.html">Order history detail</a>
                                                </li>
                                                <li><a href="customer-wishlist.html">Wishlist</a>
                                                </li>
                                                <li><a href="customer-account.html">Customer account / change password</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Order process</h5>
                                            <ul>
                                                <li><a href="basket.html">Shopping cart</a>
                                                </li>
                                                <li><a href="checkout1.html">Checkout - step 1</a>
                                                </li>
                                                <li><a href="checkout2.html">Checkout - step 2</a>
                                                </li>
                                                <li><a href="checkout3.html">Checkout - step 3</a>
                                                </li>
                                                <li><a href="checkout4.html">Checkout - step 4</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>Pages and blog</h5>
                                            <ul>
                                                <li><a href="blog.html">Blog listing</a>
                                                </li>
                                                <li><a href="post.html">Blog Post</a>
                                                </li>
                                                <li><a href="faq.html">FAQ</a>
                                                </li>
                                                <li><a href="text.html">Text page</a>
                                                </li>
                                                <li><a href="text-right.html">Text page - right sidebar</a>
                                                </li>
                                                <li><a href="404.html">404 page</a>
                                                </li>
                                                <li><a href="contact.html">Contact</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                 /.yamm-content
                            </li>
                        </ul> -->
                    <!-- </li> -->
                <!-- </ul> -->

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                 <!-- <div class="navbar-collapse collapse right" id="basket-overview">
                   <a href="basket.html" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm">3 items in cart</span></a>
                </div> -->
                <!--/.nav-collapse -->

                <div class="navbar-collapse collapse right" id="search-not-mobile">
                      <!-- <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search"> -->
                      <div class="navbar-btn btn-primary">
                        <script src="https://apis.google.com/js/platform.js"></script>
                          <div class="g-ytsubscribe" data-channel="SuperWilangel" data-layout="default" data-count="default"></div>
                        <!-- <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i> -->
                      </div>
                    <!-- </button> -->
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

		    </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->
