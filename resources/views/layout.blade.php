<!DOCTYPE html>
<html lang="en">

<head>
    <!-- #751008 Bordeau-->
    <!-- #D4D83F Vert nuance comme pour vie de dingue-->
    <meta charset="utf-8">
    <meta name="robots"         content="all,follow">
    <meta name="googlebot"      content="index,follow,snippet,archive">
    <meta name="viewport"       content="width=device-width, initial-scale=1">
    <meta name="description"    content="Afrodidacte, a plateforme pour que les Africains apprennent de leur histoire et cree de la valeur">
    <meta name="keywords"       content="afrodidacte,histoire,valeur,richesse,developpement personnel,">

    <title>
        Afrodidacte | Motivation - Inspiration - Liberté
    </title>
    @yield('article_meta')
    <link href="css/bulma.css" rel="stylesheet">
    <link href="css/vitabu.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{asset('css/font-awesome.css')}}"  rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/animate.min.css')}}"   rel="stylesheet" type="text/css">
    <link href="{{asset('css/owl.carousel.css')}}"  rel="stylesheet" type="text/css">
    <link href="{{asset('css/owl.theme.css')}}"     rel="stylesheet" type="text/css">
    <link href="{{asset('css/bulma.css')}}"         rel="stylesheet" type="text/css">
    <!-- theme stylesheet -->
    <link href="{{asset('css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">
    <!-- youmax -->
    <link href="{{asset('css/youmax.min.css')}}"    rel="stylesheet" type="text/css">
    <!-- your stylesheet with modifications -->
    <link href="{{asset('css/custom.css')}}"        rel="stylesheet">

    <script src="{{asset('js/respond.min.js')}}"></script>

    <!-- <link rel="shortcut icon" href="favicon.png"> -->
    <!-- <link rel="stylesheet" href="css/mediaelementplayer.min.css" /> -->


    <!-- mobile -->
    <!-- <script>

    if(navigator.userAgent.match(/Android|iPhone/i) && !navigator.userAgent.match(/iPad/i)) {
      document.head.insertAdjacentHTML( 'beforeEnd', '<link href="css/mobile.css" rel="stylesheet">');
      }

    </script> -->

</head>

<body>

    <!-- *** TOPBAR ***
 _________________________________________________________ -->
    <!-- <div id="top">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
                <a href="http://eepurl.com/c6W3u5" class="btn btn-success btn-sm" data-animate-hover="shake">Devenez un Afrodidacte maintenant</a>
                 <a href="#">Get flat 35% off on orders over $50!</a>
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <li>
                       <a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                       Youtube Button
                      <script src="https://apis.google.com/js/platform.js"></script>
                        <div class="g-ytsubscribe" data-channel="SuperWilangel" data-layout="default" data-count="default"></div>
                    </li>
                     <li><a href="register.html">Register</a>
                    </li>
                    <li><a href="contact.html">Contact</a>
                    </li>
                    <li><a href="#">English</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Customer login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="customer-orders.html" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email-modal" placeholder="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password-modal" placeholder="password">
                            </div>

                            <p class="text-center">
                                <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>

                        </form>

                        <p class="text-center text-muted">Not registered yet?</p>
                        <p class="text-center text-muted"><a href="register.html"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->
    @include('nav')


    <div id="all">

        @yield('content')
        <!-- /#content -->

        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-3 col-sm-6">
                        <h4>Pages</h4>

                        <ul>
                            <li><a href="text.html">About us</a>
                            </li>
                            <li><a href="text.html">Terms and conditions</a>
                            </li>
                            <li><a href="faq.html">FAQ</a>
                            </li>
                            <li><a href="contact.html">Contact us</a>
                            </li>
                        </ul>

                        <hr>

                        <h4>User section</h4>

                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                            </li>
                            <li><a href="register.html">Regiter</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div> -->
                    <!-- /.col-md-3 -->

                    <!-- <div class="col-md-3 col-sm-6">

                        <h4>Top categories</h4>

                        <h5>Men</h5>

                        <ul>
                            <li><a href="category.html">T-shirts</a>
                            </li>
                            <li><a href="category.html">Shirts</a>
                            </li>
                            <li><a href="category.html">Accessories</a>
                            </li>
                        </ul>

                        <h5>Ladies</h5>
                        <ul>
                            <li><a href="category.html">T-shirts</a>
                            </li>
                            <li><a href="category.html">Skirts</a>
                            </li>
                            <li><a href="category.html">Pants</a>
                            </li>
                            <li><a href="category.html">Accessories</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg">

                    </div> -->
                    <!-- /.col-md-3 -->

                    <!-- <div class="col-md-3 col-sm-6">

                        <h4>Where to find us</h4>

                        <p><strong>Obaju Ltd.</strong>
                            <br>13/25 New Avenue
                            <br>New Heaven
                            <br>45Y 73J
                            <br>England
                            <br>
                            <strong>Great Britain</strong>
                        </p>

                        <a href="contact.html">Go to contact page</a>

                        <hr class="hidden-md hidden-lg">

                    </div> -->
                    <!-- /.col-md-3 -->



                    <!-- <div class="col-md-3 col-sm-6"> -->

                        <!-- <h4>Get the news</h4>

                        <p class="text-muted">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

                        <form>
                            <div class="input-group">

                                <input type="text" class="form-control">

                                <span class="input-group-btn">

			    <button class="btn btn-default" type="button">Subscribe!</button>

			</span> -->

                            <!-- </div> -->
                            <!-- /input-group -->
                        <!-- </form> -->



                        <!-- <h4>Restons en contact</h4> -->

                        <!-- <p class="social"> -->
                            <!-- <a href="https://www.facebook.com/Afrodidacte/" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.youtube.com/c/afrodidacte" class="youtube external" data-animate-hover="shake"><i class="fa fa-youtube"></i></a> -->
                            <!-- <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a> -->
                            <!-- <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a> -->
                            <!-- <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a> -->
                        <!-- </p> -->


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <!-- <div class="container"> -->
                <!-- <div class="col-md-6"> -->
                    <p>© 2017 Afrodidacte
                      <a href="https://www.facebook.com/Afrodidacte/" class="facebook external" data-animate-hover="shake"><i class="fa fa-4x fa-facebook"></i></a>
                      <a href="https://www.youtube.com/c/afrodidacte" class="youtube external" data-animate-hover="shake"><i class="fa fa-4x fa-youtube"></i></a>
                       <a href="https://yanfoma.tech">From the hotpot of technologies.</a></p>

                <!-- </div> -->
                <div class="col-md-6">
                    <!-- <p class="pull-right">Design by <a href="https://bootstrapious.com/e-commerce-templates">Bootstrapious</a> & <a href="https://fity.cz">Fity</a> -->
                         <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
                    <!-- </p> -->
                </div>
            <!-- </div> -->
        </div>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->




    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- <script src="js/front.js"></script> -->
    <!-- <script src="js/mediaelement-and-player.min.js"></script> -->
    <!-- <script>
        var player = new MediaElementPlayer('#player1');
    </script> -->
    <script>
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '563',
          width: '1200',
          videoId: '6DidpsFhZps',
          events: {
            'onReady': onPlayerReady,
            // 'onStateChange': onPlayerStateChange
          }
        });
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      // var done = false;
      // function onPlayerStateChange(event) {
      //   if (event.data == YT.PlayerState.PLAYING && !done) {
      //     setTimeout(stopVideo, 6000);
      //     done = true;
      //   }
      // }
      // function stopVideo() {
      //   player.stopVideo();
      // }
    </script>
    <!-- Youmax Call -->
    <script src="js/src/magnific-popup-iframe-gallery.js"></script>
    <script src="js/src/youmax.js"></script>
    <script>

        $(document).ready(function(){

            $(".youmax").youmax({
                // apiKey:"AIzaSyA5Pl9sV1YSCzZSrdDL4qPi5LnInCj7edM",

                channelLink:"https://www.youtube.com/user/SuperWilangel",
                playlistLink:"https://www.youtube.com/watch?v=Iodtnt2H3h0&list=PLN611u-f43bM-L0rt-A2LZv0x914ZjqBK",

                defaultTab:"Récents",         //Uploads|Playlists|Featured
                videoDisplayMode:"popup",       //popup|link|inline
                maxResults:"9",
                autoPlay:true,
                displayFirstVideoOnLoad:true,       //for inline video display mode only

                responsiveBreakpoints   :[600,900,2000,2500],

                loadMoreText            :"<i class=\"fa fa-plus\"></i>&nbsp;&nbsp;Plus de videos ...",
                previousButtonText      :"<i class=\"fa fa-angle-left\"></i>&nbsp;&nbsp;Precedent",
                nextButtonText          :"Suivant&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i>",
                loadingText             :"loading...",
                allDoneText             :"<i class=\"fa fa-times\"></i>&nbsp;&nbsp;Done..",

                hideHeader              :false,
                hideTabs                :false,
                hideLoadingMechanism    :false,


            });
        });


    </script>
</body>

</html>
