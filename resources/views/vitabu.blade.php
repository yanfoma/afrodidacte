<style>
.product_space{
    margin-bottom: 20px;
}

</style>
@extends('layout')

@section('content')


<div id="content">

  <div class="container">
    @if($vitabus->count())
      <h2 style="color: #751008" >Vitabu</h2>

      <hr style="height:0.5px; background-color: #751008;"/>
      <div class="row products formation_line">
      @foreach($vitabus as $vitabu)

          <div class='col-md-3 col-sm-4 product_space center_content'>
              <div class="card">
                  <a class="container-card-top" href="{{$vitabu->lien}}" target="_blank">
                    <img border="0" src="{{$vitabu->image}}" >
                  </a>
                  <div class="container_card">
                      <h4><b>{{$vitabu->titre}} </b></h4>
                      <p>{{$vitabu->auteur}}</p>
                      <a class='btn btn-primary' target="_blank" href="{{$vitabu->lien}}">Acheter</a>
                  </div>
              </div>
            </div>

          @endforeach
          </div>
        </div>

        @else
            <h2 style="color: #751008" >Aucun Vitabu</h2>

            <hr style="height:0.5px; background-color: #751008;"/>
        @endif

    </div>
</div>

@stop
